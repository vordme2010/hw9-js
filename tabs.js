const tabs = document.querySelector("ul")
tabs.addEventListener("click", event => {
    if(event.target.classList.contains("tabsTitleJs") && !event.target.classList.contains("active")) {
        const tabsActive = document.querySelectorAll(".active")
        const tabId = event.target.getAttribute("data-tab")
        const currentTabText = document.querySelector(tabId)
        tabsActive.forEach( elem => {
            elem.classList.remove("active")
        }) 
        currentTabText.classList.add("active") 
        event.target.classList.add("active")
    }
}) 